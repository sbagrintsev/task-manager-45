package ru.tsc.bagrintsev.tm.dto.request.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.request.AbstractUserRequest;

@NoArgsConstructor
public final class DataBase64LoadRequest extends AbstractUserRequest {

    public DataBase64LoadRequest(@Nullable String token) {
        super(token);
    }

}
