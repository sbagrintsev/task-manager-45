package ru.tsc.bagrintsev.tm.api.sevice;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.api.sevice.dto.IProjectServiceDTO;
import ru.tsc.bagrintsev.tm.api.sevice.dto.IProjectTaskServiceDTO;
import ru.tsc.bagrintsev.tm.api.sevice.dto.ITaskServiceDTO;
import ru.tsc.bagrintsev.tm.api.sevice.dto.IUserServiceDTO;

public interface IServiceLocator {

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IDomainService getDomainService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IProjectServiceDTO getProjectService();

    @NotNull
    IProjectTaskServiceDTO getProjectTaskService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ITaskServiceDTO getTaskService();

    @NotNull
    IUserServiceDTO getUserService();

}
